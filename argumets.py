# -*- coding:utf-8 -*-

def func(kind, *argv, **keywords):
    print "-- Do you have any", kind, "?"
    print "-- I'm sorry, we're all out of", kind
    for arg in argv:
        print arg
    print "-" * 40
    keys = sorted(keywords.keys())
    for kw in keys:
        print kw, ":", keywords[kw]

#不能直接通过list和dict变量传参
# li = ["It's very runny, sir.","It's really very, VERY runny, sir."]
# dic = { 'shopkeeper':'Michael Palin',
#         'client':"John Cleese",
#         'sketch':"Cheese Shop Sketch"}

# func("limburger", li, dic)

func("Limburger", "It's very runny, sir.",
           "It's really very, VERY runny, sir.",
           shopkeeper='Michael Palin',
           client="John Cleese",
           sketch="Cheese Shop Sketch")