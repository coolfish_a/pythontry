# -*- coding:utf-8 -*-

import urllib
def reporthook(bk,bs,total): 
    "默认下载1024*8字节回调一次，也就是bs大小，blocknum是块数量，其实就是回调的次数，size是下载文件总大小"
    print 'bs:', bs, 'bk:', bk, bk*bs, 'b'

filename, message=urllib.urlretrieve("http://ww.baidu.com/", None, reporthook)
print "-------------------"
print message.getheader('Content-Length')