# --*-- coding:utf-8 --*--
import json


class Student(object):

    def __init__(self, name, age, city):
        self.name = name
        self.age = age
        self.city = city

def student2dict(obj):
        return {'name': obj.name, 'age': obj.age, 'city': obj.city}

s = Student('wxf', '19', 'ah')
print(json.dumps(s, default=student2dict))
