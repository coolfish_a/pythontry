# --*--coding:utf-8--*--
import sqlite3

try:
    conn = sqlite3.connect("test.db")
    cursor = conn.cursor()
    # cursor.execute(
    #     'create table user (id varchar(20) primary key, name varchar(20))')
    
    #插入单条数据
    # cursor.execute("insert into user (id, name) values ('3', 'john')")
    
    #插入多条数据
#    cursor.execute("insert into user (id, name) select '6', 'pig' union all select '7', 'li'")
#    print cursor.rowcount
    
    #取全部数据
#    cursor.execute('select * from user')

    #取多条数据
    # cursor.execute('select * from user where id=? or id=?',('1','2'))
    cursor.execute('select * from user where id in (?,?)',('1','2'))
    values = cursor.fetchall()
    print 'values:', values
except Exception, e:
    print 'Exception:', e
finally:
    cursor.close()
    conn.commit()
    conn.close()
