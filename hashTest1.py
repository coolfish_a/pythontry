# -*- coding:utf-8 -*-
class B:
	def __init__(self,name):
		self.name=name
	def __hash__(self):
		return hash(self.name)
	def __eq__(self,r):  #lookdict的部分源码 只有hash值一样且key值相等才有更新
		if self.name == r.name:
			return True
		else:
			return False
d={}
b1=B('skycrab')
b2=B('skycrab1')
print b1.__hash__
print '==========='
print b2.__hash__
print '==========='
d[b1]=1
d[b2]=2
b2.name='skycrab' #与b1 kye值相等
d[b2]=3           #将更新d[b1]的值
print d
