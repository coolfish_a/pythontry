# err.py
def foo(s):
    n = int(s)
    return 10 / n


def bar(s):
    try:
        return foo(s) * 2
    except StandardError, e:
        print 'Error!'
        raise  # 计较注释掉raise的区别，上抛错误


def main():
    bar('0')

main()
