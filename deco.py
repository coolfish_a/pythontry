# -*- coding:utf-8 -*-
'''示例6: 对参数数量不确定的函数进行装饰，
参数用(*args, **kwargs)，自动适应变参和命名参数'''

import functools
"带参数装饰函数"
def deco(arg):
    def _deco(func):
        @functools.wraps(func)
        def log():
            print("before %s called [%s]." % (func.__name__, arg))
            func()
            print("  after %s called [%s]." % (func.__name__, arg))
        return log
    return _deco

# "不带参数装饰函数"
# def deco(func):
#     def _deco():
#         print("before %s called." % (func.__name__))
#         func()
#         print("  after %s called." % (func.__name__))
#     return _deco

@deco("mymodule")
def myfunc():
    print(" myfunc() called.")

@deco("module2")
def myfunc2():
    print(" myfunc2() called.")

myfunc()
myfunc2()
print myfunc.__name__

# def deco3(func):
#     def _deco(*args, **kwargs):
#         print("before %s called." % func.__name__)
#         ret = func(*args, **kwargs)
#         print("  after %s called. result: %s" % (func.__name__, ret))
#         return ret
#     return _deco

# @deco3
# def myfunc4(a, b):
#     print(" myfunc(%s,%s) called." % (a, b))
#     return a+b

# @deco3
# def myfunc5(a, b, c):
#     print(" myfunc2(%s,%s,%s) called." % (a, b, c))
#     return a+b+c

# myfunc4(1, 2)
# myfunc4(3, 4)
# myfunc5(1, 2, 3)
# myfunc5(3, 4, 5)
