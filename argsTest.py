# -*- coding: UTF-8 -*- 

class Thing(object):
    def test(hi):
        print hi

a = Thing()
'''test()只接受一个参数，有一个默认的self，调用时传参‘Hello’，实际是传了两个参数导致错误发生'''
# a.test("Hello") 

#不明确传参的情况下，实际接收了一个self参数
a.test()