# -*- coding: utf-8 -*-

html = """
<html><head><title>The Dormouse's story</title></head>
<body>
<p class="title" name="dromouse"><b>The Dormouse's story</b></p>
<p class="story">Once upon a time there were three little sisters; and their names were
<a href="http://example.com/elsie" class="sister" id="link1"><!-- Elsie --></a>,
<a href="http://example.com/lacie" class="sister" id="link2">Lacie</a> and
<a href="http://example.com/tillie" class="sister" id="link3">Tillie</a>;
and they lived at the bottom of a well.</p>
<p class="story">...</p>
"""  
import bs4
from bs4 import BeautifulSoup 

soup = BeautifulSoup(html, "html.parser")
# soup = BeautifulSoup(open('index.html')) 
print "==>soup.prettify():",soup.prettify()
print "==>soup.name:",soup.name
print "==>soup.head.name:",soup.head.name
print "==>soup.head:",soup.head
print "==>soup.title:",soup.title
print "==>soup.p:",soup.p
print "==>soup.p.attrs:",soup.p.attrs
print "==>soup.p['class']:",soup.p['class'],"\t","soup.p['name']:",soup.p['name']
print "==>soup.p.string:",soup.p.string
print "==>type(soup.p.string):",type(soup.p.string)
print "==>soup.a:",soup.a 
print "==>type(soup.a):",type(soup.a)
if type(soup.a.string)==bs4.element.Comment:
    print "==>soup.a.string:",soup.a.string 

for child in  soup.body.children:
    print child

print "=============="

for child in soup.descendants:
    print child  
    print "---------"

for string in soup.strings:
    print(repr(string))
print "=============="
#去除多余的空格
for string in soup.stripped_strings:
    print(repr(string))

print "=============="
print soup.find_all('p') 