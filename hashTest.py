# -*- coding:utf-8 -*-
'''与hashTest1.py进行比较'''
class B:
	def __init__(self,name):
		self.name=name
	def __hash__(self):
		return hash(self.name)
	
d={}
b1=B('skycrab')
b2=B('skycrab1')
print b1.__hash__
print '==========='
print b2.__hash__
print '==========='
d[b1]=1
d[b2]=2
b2.name='skycrab'
d[b2]=3
print d