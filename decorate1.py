# -*- coding:utf-8 -*-

from contextlib import contextmanager
from contextlib import nested
from contextlib import closing
'''contextlib还有连个重要的东西，一个是nested，一个是closing，前者用于创建嵌套的上下文，后则用于帮你执行定义好的close函数。但是nested已经过时了，因为with已经可以通过多个上下文的直接嵌套了。'''
@contextmanager
def make_context(name) :
    print 'enter', name
    yield name
    print 'exit', name
  
with nested(make_context('A'), make_context('B')) as (a, b) :
    print a
    print b
  
with make_context('A') as a, make_context('B') as b :
    print a
    print b
  
class Door(object) :
    def open(self) :
        print 'Door is opened'
    def close(self) :
        print 'Door is closed'
  
with closing(Door()) as door :
    door.open()