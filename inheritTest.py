 
'''-----新式类和旧式类中查找属性的顺序不同------
在新式类中，查找一个要调用的函数或者属性的时候，是广度优先搜搜的。
在旧式类当中，是深度优先搜索的。
新式类 class D(object) A-->B-->C-->D-->object  输出：class C
旧式类 class D()  A-->B-->D-->C-->D   输出：class D'''
#class D(object):
class D():
    def foo(self):
        print "class D"

class B(D):
    pass

class C(D):
    def foo(self):
        print "class C"

class A(B, C):
    pass

f = A()
f.foo()