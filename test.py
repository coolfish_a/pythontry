#!/usr/bin/python
# -*- coding: utf-8 -*-

def varargs(*args):
    return args

def keyword_args(**kwargs):
    return kwargs

class Human(object):
 
    # A class attribute. It is shared by all instances of this class
    # 下面是一个类属性。它将被这个类的所有实例共享。
    species = "H. sapiens"
 
    # Basic initializer
    # 基本的初始化函数（构造函数）
    def __init__(self, name):
        ''' Assign the argument to the instance's name attribute
        把参数赋值为 name的属性'''
        self.name = name
 
    # An instance method. All methods take self as the first argument
    # 下面是一个实例方法。所有方法都以 self 作为第一个参数。
    def say(self, msg):
       return "%s: %s" % (self.name, msg)
 
    # A class method is shared among all instances
    # They are called with the calling class as the first argument
    # 类方法会被所有实例共享。
    # 类方法在调用时，会将类本身作为第一个函数传入。
    @classmethod
    def get_species(cls):
        return cls.species
    
    # A static method is called without a class or instance reference
    # 静态方法在调用时，不会传入类或实例的引用。
    @staticmethod
    def grunt():
        return "*grunt*"
 
 
print varargs('a','b')
print keyword_args(a=1)
# Instantiate a class
# 实例化一个类
i = Human(name="Ian")
print i.say("Hi")     # prints out "Ian: Hi"
                      # 打印出 "Ian: Hi"
 
j = Human("Joel")
print j.say("hello")  # prints out "Joel: hello"
                      # 打印出 "Joel: hello"
 
# Call our class method
# 调用我们的类方法
print i.get_species() #=> "H. sapiens"
 
# Change the shared attribute
# 修改共享属性
Human.species = "H. neanderthalensis"
print i.get_species() #=> "H. neanderthalensis"
print j.get_species() #=> "H. neanderthalensis"
 
# Call the static method
# 调用静态方法
print Human.grunt() #=> "*grunt*"
print Human.__init__.__doc__


        