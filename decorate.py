# -*- coding:utf-8 -*-

from contextlib import contextmanager
@contextmanager
def myfile(name):
    f=open(name)
    try:
        yield f
    finally:
        print 'finally'
        f.close()

with myfile('mydata.txt') as f:
    for x in f:
        print x