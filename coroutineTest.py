# -*- coding:utf-8 -*-
import time

def consumer():
    r = ''
    while True:
        n = yield r
        print 'n:',n,'r:',r
        if not n:
            return
        print('[CONSUMER] Consuming %s...' % n)
        time.sleep(1)
        r = '200 OK'

def produce(c):
    c.next()
    n = 0
    while n < 5:
        n = n + 1
        print('[PRODUCER] Producing %s...' % n)
        z = c.send(n)
        print 'n:',n,'z:',z
        print('[PRODUCER] Consumer return: %s' % z)
    c.close()

if __name__=='__main__':
    c = consumer()
    produce(c)
