# -*- coding: cp936 -*-
import time
import urllib2
from multiprocessing.dummy import Pool as ThreadPool

urls = [
    'http://www.baidu.com',
    'http://www.python.org',
    'http://www.python.org/doc',
    'http://www.python.org/download/',
    'http://www.python.org/getit',
    'http://www.python.org/community/'
    ]

pool = ThreadPool(4)
start_time = time.time()
results  = pool.map(urllib2.urlopen, urls)
pool.close()
pool.join()
print 'Done! Time taken:{}'.format(time.time() - start_time)


#�ο������д��о���http://www.oschina.net/translate/python-parallelism-in-one-line
