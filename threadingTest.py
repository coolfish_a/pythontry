# -*- coding: cp936 -*-
import threading
import re
import time
import datetime

class MyThread(threading.Thread):
      def __init__(self,id):
             threading.Thread.__init__(self)
             self.id=id
      def run(self):
            p = re.compile(r'^(?i)kt.*', re.DOTALL)
            m = p.match('KT hello world! \
                sgafg ag \n�յ� ')
     
            for i in xrange(12000000):
                if m:
                    #print i#,m.group()
                    continue
                    print "self.id:",self.id

def test():
    d1 = datetime.datetime.now()
    
    thread1 = MyThread(1)
    thread2 = MyThread(2)
    thread3 = MyThread(3)
    thread4 = MyThread(4)
    thread5 = MyThread(5)
    thread1.start()
    thread2.start()
    thread3.start()
    thread4.start()
    thread5.start()
    thread1.join()
    thread2.join()
    thread3.join()
    thread4.join()
    thread5.join()

    d2 = datetime.datetime.now()
    print "usetime:",str((d2 - d1).seconds)+"s"+str((d2 - d1).microseconds/1000)+"ms"
    #print "usetime:",

if __name__ == '__main__':
    test()
