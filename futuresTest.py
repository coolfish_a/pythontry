#http://blog.chinaunix.net/uid-21553594-id-4033977.html  python ������
#��Ҫpython3.2

#!/usr/bin/python
# -*- coding: gbk -*-  

from concurrent.futures import *
import time
import threading
import concurrent
import math

def add_print(a, b):
    time.sleep(.5)
    print(a + b, threading.current_thread())
    return (a+b)
    
def slow_print(x):
    print(x, threading.current_thread())
    time.sleep(.5) 
    return (x)


def test1():
    fs = []
    e = ThreadPoolExecutor(max_workers=3)
    for i in range(1, 10):
        print(i)
        f = e.submit(add_print, i, i)
        print(f.done())
        fs.append(f)
        #print(f.cancel())
    e.shutdown(False)
    concurrent.futures.wait(fs)
    print('done')

def test3():
    e = ThreadPoolExecutor(max_workers=3)
    fs = e.map(slow_print, range(1, 10))
    time.sleep(0.1)
    print('before')
    for f in fs:
        print(f)
    print('after')
    
    

def test2():
    #print(pow(323, 123))
    with ThreadPoolExecutor(max_workers=1) as executor:
        future = executor.submit(pow, 323, 12356666665)
        while future.running():
            time.sleep(0.01)
            print('wait')
        print(future.result())
        print(future.running())
        print(future.done())
        print(future.result())
        print(future.exception())


PRIMES = [
    112272535095293,
    112582705942171,
    112272535095293,
    115280095190773,
    115797848077099,
    1099726899285419]

def is_prime(n):
    if n % 2 == 0:
        return False

    sqrt_n = int(math.floor(math.sqrt(n)))
    for i in range(3, sqrt_n + 1, 2):
        if n % i == 0:
            return False
    return True

def test4():
    with concurrent.futures.ProcessPoolExecutor() as executor:
        for number, prime in zip(PRIMES, executor.map(is_prime, PRIMES)):
            print('%d is prime: %s' % (number, prime))


if __name__ == '__main__':
    test4()
