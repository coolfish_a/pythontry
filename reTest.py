# -*- coding: utf-8 -*-
#一个简单的match实例

import re

pattern = re.compile(r'\d+')
#两种调用方式
print pattern.split('one1two2three3four4')
print re.split(pattern,'one1two2three3four4')

print "==================================="
patt = re.compile(r'(\w+) (\w+)')
s = 'i say, hello world!'

print re.match(patt, s).string

print re.sub(patt,r'\2 \1', s)
print re.subn(patt,r'\2 \1', s)
#当 repl 是一个方法时，这个方法应当只接受一个参数（Match对象），并返回一个字符串用于替换（返回的字符串中不能再引用分组）
def func(m):
    return m.group(1).title() + ' ' + m.group(2).title()

print re.sub(patt,func, s)
print re.subn(patt,func, s)

print "==================================="
# 匹配如下内容：单词+空格+单词+任意字符
m = re.match(r'(\w+) (\w+)(.*)', 'hello world!')
print "m.string:", m.string
print "m.re:", m.re
print "m.pos:", m.pos
print "m.endpos:", m.endpos
print "m.lastindex:", m.lastindex
print "m.lastgroup:", m.lastgroup
print "m.group():", m.group()
print "m.group(1,2):", m.group(1, 2)
print "m.groups():", m.groups()
print "m.groupdict():", m.groupdict()
print "m.start(2):", m.start(2)
print "m.end(2):", m.end(2)
print "m.span(2):", m.span(2)
print r"m.expand(r'\g \g\g'):", m.expand(r'\2 \1\3')

### output ###
#['one', 'two', 'three', 'four', '']
# m.string: hello world!
# m.re: 
# m.pos: 0
# m.endpos: 12
# m.lastindex: 3
# m.lastgroup: None
# m.group(1,2): ('hello', 'world')
# m.groups(): ('hello', 'world', '!')
# m.groupdict(): {}
# m.start(2): 6
# m.end(2): 11
# m.span(2): (6, 11)
# m.expand(r'\2 \1\3'): world hello! 