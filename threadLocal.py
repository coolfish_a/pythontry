# -*- coding:utf-8 -*-
import threading

# 创建全局ThreadLocal对象
local_school = threading.local()


def process_student():
    print 'Hello, %s (in %s)' % (local_school.s,
                                 threading.current_thread().name)


def process_thread(name):
    # 绑定threadLocal的student student只代表一个变量
    local_school.s = name
    process_student()

t1 = threading.Thread(target=process_thread, args=('Alice',), name='Thread-A')
t2 = threading.Thread(target=process_thread, args=('Bob',), name='Thread-B')
t1.start()
t2.start()
t1.join()
t2.join()
